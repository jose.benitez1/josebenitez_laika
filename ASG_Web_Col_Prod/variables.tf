variable "region" {
  type = string
  default = "us-east-2"
}

variable "profile_aws" {
  type = string
  default = "Laika_Prod"
}

variable "environment" {
  type = string
  default = ""
}