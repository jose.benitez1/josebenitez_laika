#Importar librerias
import mysql.connector
import datetime
import pandas as pd
import pysftp as sftp

# Variables de fecha
ahora = datetime.datetime.now().date()
hoy = str(ahora)
# year = hoy.split("-")[0] # deuda tecnica
year = "21"
mes = hoy.split("-")[1]
dia = hoy.split("-")[2]

# Creación del nombre archivo.txt
Name_txt = "Inventario"+" "+dia+" "+mes+" "+year+"-4.txt"

# Conexión a la DB
Laikapp_Testing = mysql.connector.connect(host= 'laika-production-v2-read-3.c401c5lcer4y.us-east-2.rds.amazonaws.com',
                                          user= 'devops', password= 'dev2398*ops', database = 'laika')

# Realiza query a la tabla de la db
mysql_query = pd.read_sql_query('''SELECT DISTINCT r.sku Referencia_Aliado, r.sku EAN, p.`name` Nombre, p.description Descripcion, b.`name` Marca, wr.stock Stock,
CASE 
    WHEN c.id =1  THEN '900034380'
    WHEN c.id =2  THEN '900049032'
	  WHEN c.id =154  THEN '900084676'
    WHEN c.id =122  THEN '900121843'
    WHEN c.id =93  THEN '900125978'
		WHEN c.id =659  THEN '900134223'
   	ELSE ""
END  Tienda,
wr.sale_price Precio_Por_Tienda, wr.sale_price Precio_Con_Descuento, "" Descuento, ""  Fecha_Inicio_Descuento ,
"" Fecha_Fin_Descuento,"Mascotas"  Categoria_Producto_1, pe.`name` Categoria_Producto_2, ca.`name` Categoria_Producto_3,  typ.`name` Categoria_Producto_4, ri.url Imagen_de_Producto, "" Categoria_Combinacion, "" Nombre_Combinacion
FROM wh_reference_trading_platform wrtp
LEFT JOIN warehouse_references wr ON wr.id = wrtp.warehouse_reference_id 
LEFT JOIN `references` r ON r.id = wr.reference_id
LEFT JOIN products p ON p.id = r.product_id
LEFT JOIN brand_products bp ON bp.product_id = p.id 
LEFT JOIN type_product_brands tpb ON  tpb.id = bp.type_product_brand_id
LEFT JOIN brands b ON b.id = tpb.brand_id 
LEFT JOIN warehouses w ON w.id = wr.warehouse_id 
LEFT JOIN cities c ON c.id = w.city_id 
LEFT JOIN category_type_products ctp ON ctp.id = tpb.category_type_product_id 
LEFT JOIN category_cities ct ON ct.id = ctp.category_city_id 
LEFT JOIN category_pets cp ON cp.id = ct.category_pet_id
LEFT JOIN pets pe ON pe.id = cp.pet_id
LEFT JOIN categories ca ON ca.id = cp.category_id
LEFT JOIN type_products typ ON typ.id = ctp.type_product_id 
LEFT JOIN references_images ri ON ri.reference_id = r.id 
WHERE wrtp.other_trading_platform_id = 2 AND bp.status_id = 1 and tpb.status_id = 1 and ctp.status_id = 1 
and ct.status_id = 1 and cp.status_id = 1 and c.id in (1,2,154,122,93,659) and wr.stock > 5 AND wrtp.status_id = 1
and wr.sale_price > 0 and wr.status_id =1 
GROUP BY r.id''', Laikapp_Testing)

# Importa de xlsx a txt
df = pd.DataFrame(mysql_query)
df.to_csv(Name_txt, sep="\t", index=False)
Laikapp_Testing.close()

def push_file_to_server():
    cnopts = sftp.CnOpts()
    cnopts.hostkeys = None
    s = sftp.Connection(host='sftp-allies.rappi.com', username='laika_co', private_key='key_rappi.pem', cnopts=cnopts)
    print(s)
    local_path = Name_txt
    remote_path = "/"+Name_txt

    s.put(local_path, remote_path)
    s.close()

push_file_to_server()
