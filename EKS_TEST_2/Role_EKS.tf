# Policy and Service for EKS Cluster
resource "aws_iam_role" "EKS_Role_Cluster_Testing" {
  name = "EKSRoleClusterTesting"
  assume_role_policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Action": "sts:AssumeRole",
      "Principal": {
        "Service": "eks.amazonaws.com"
      },
      "Effect": "Allow",
      "Sid": ""
    }
  ]
}
EOF
}

resource "aws_iam_role_policy_attachment" "Policy_EKS_Role_Cluster_Testing" {
  policy_arn = var.AmazonEKSClusterPolicy
  role = aws_iam_role.EKS_Role_Cluster_Testing.name
}

resource "aws_iam_role_policy_attachment" "Service_EKS_Role_Cluster_Testing" {
  policy_arn = var.AmazonEKSServicePolicy
  role = aws_iam_role.EKS_Role_Cluster_Testing.name
}
###################################################################################

# Policy and Service for EKS_Node_Group Cluster
resource "aws_iam_role" "Role_EKS_Node_Group" {
  name  = "EKSNodeGroupClusterLaika"
  assume_role_policy = <<POLICY
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Effect": "Allow",
      "Principal": {
        "Service": "ec2.amazonaws.com"
      },
      "Action": "sts:AssumeRole"
    }
  ]
}
POLICY
}

resource "aws_iam_role_policy_attachment" "Policy_AmazonEKSWorkerNodePolicy" {
  policy_arn = "arn:aws:iam::aws:policy/AmazonEKSWorkerNodePolicy"
  role       = aws_iam_role.Role_EKS_Node_Group.name
}

resource "aws_iam_role_policy_attachment" "Policy_AmazonEKS_CNI_Policy" {
  policy_arn = "arn:aws:iam::aws:policy/AmazonEKS_CNI_Policy"
  role       = aws_iam_role.Role_EKS_Node_Group.name
}

resource "aws_iam_role_policy_attachment" "Policy_AmazonEC2ContainerRegistryReadOnly" {
  policy_arn = "arn:aws:iam::aws:policy/AmazonEC2ContainerRegistryReadOnly"
  role       = aws_iam_role.Role_EKS_Node_Group.name
}

resource "aws_iam_instance_profile" "Instance_Profile_Node_EKS" {
  name = "instance-profile-node-eks"
  role = aws_iam_role.Role_EKS_Node_Group.name
}
#######################################################################################################