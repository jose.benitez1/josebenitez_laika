variable "AmazonEKSClusterPolicy" {
  type = string
  default = "arn:aws:iam::aws:policy/AmazonEKSClusterPolicy"
}

variable "AmazonEKSServicePolicy" {
  type = string
  default = "arn:aws:iam::aws:policy/AmazonEKSServicePolicy"
}

variable "Version_EKS_Cluster" {
  type = string
  default = "1.20"
}

variable "cluster-name" {
  default = "Kubernetes_Laika_Testing"
  type = string
}

variable "VPC_Id" {
  type = string
  default = "vpc-7c6cbf14"
}