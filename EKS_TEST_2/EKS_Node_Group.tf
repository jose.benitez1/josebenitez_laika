resource "aws_eks_node_group" "Node_Cluster_Laika_Testing" {
  cluster_name = aws_eks_cluster.Kubernetes_Laika_Testing.name
  node_group_name = "Node_Cluster_Laika_Testing"
  node_role_arn = aws_iam_role.Role_EKS_Node_Group.arn
  subnet_ids = ["subnet-ec687597", "subnet-6daafc20", "subnet-7488531c"]

  tags = {
    Name = "Node_Cluster_Laika_Testing"
    "kubernetes.io/cluster/${aws_eks_cluster.Kubernetes_Laika_Testing.name}" = "shared"
  }
  scaling_config {
    desired_size = 1
    max_size = 2
    min_size = 1
  }

  launch_template {
    name = aws_launch_template.EKS_Node_Launch_Template.name
    version = aws_launch_template.EKS_Node_Launch_Template.latest_version
  }

  depends_on = [
    aws_iam_role_policy_attachment.Policy_AmazonEKSWorkerNodePolicy,
    aws_iam_role_policy_attachment.Policy_AmazonEKS_CNI_Policy,
    aws_iam_role_policy_attachment.Policy_AmazonEC2ContainerRegistryReadOnly
  ]

  lifecycle {
    create_before_destroy = true
  }
}