resource "aws_security_group" "EKS_SecurityGroup" {
  name = "EKS_SecurityGroup"
  description = "Cluster communication with worker nodes"
  vpc_id = var.VPC_Id

  egress {
    from_port = 0
    protocol = "-1"
    to_port = 0
    cidr_blocks = ["0.0.0.0/0"]
  }
}

resource "aws_security_group" "EKS_Node_SecurityGroup" {
  name = "EKS_Node_SecurityGroup"
  vpc_id = var.VPC_Id

  egress {
    from_port = 0
    protocol = "-1"
    to_port = 0
    cidr_blocks = ["0.0.0.0/0"]
  }

  tags = map(
        "Name","EKS_Node_SecurityGroup",
        "kubernetes.io/cluster/${var.cluster-name}", "owned"
        )
}

resource "aws_security_group_rule" "EKS_Node_Ingress_Self" {
  from_port = 0
  protocol = "-1"
  security_group_id = aws_security_group.EKS_Node_SecurityGroup.id
  source_security_group_id = aws_security_group.EKS_Node_SecurityGroup.id
  to_port = 65535
  type = "ingress"
}

resource "aws_security_group_rule" "EKS_Node_Ingress_HTTPS" {
  from_port = 443
  protocol = "tpc"
  security_group_id = aws_security_group.EKS_Node_SecurityGroup.id
  source_security_group_id = aws_security_group.EKS_SecurityGroup.id
  to_port = 443
  type = "ingress"
}

resource "aws_security_group_rule" "EKS_Node_Ingress_Others" {
  from_port = 1024
  protocol = "tpc"
  security_group_id = aws_security_group.EKS_Node_SecurityGroup.id
  source_security_group_id = aws_security_group.EKS_SecurityGroup.id
  to_port = 65535
  type = "ingress"
}