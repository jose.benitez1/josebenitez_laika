resource "aws_launch_template" "EKS_Node_Launch_Template" {
  name = "EKS_Node_Launch_Template"

  vpc_security_group_ids = ["sg-0a34e4606f196b96f", "sg-0f46fa02e2a78e54e", "sg-071db46ab2437d0eb"]
  block_device_mappings {
    device_name = "/dev/xvda"
    ebs {
      volume_size = 30
      volume_type = "gp2"
      #kms_key_id = ""
      #encrypted = ""
    }
  }
  image_id = "ami-0a217fd96603e6bba"
  instance_type = "t3.medium"

  user_data = base64encode(<<-EOF
MIME-Version: 1.0
Content-Type: multipart/mixed; boundary="==MYBOUNDARY=="
--==MYBOUNDARY==
Content-Type: text/x-shellscript; charset="us-ascii"
#!/bin/bash
/etc/eks/bootstrap.sh ${aws_eks_cluster.Kubernetes_Laika_Testing.name}
--==MYBOUNDARY==--\
  EOF
  )

  tag_specifications {
    resource_type = "instance"
    tags = {
      Name = "EKS_Node_Kubernetes_Laika_Testing"
    }
  }
  key_name = "KP_laika_Jose.Benitez_Testing"
}