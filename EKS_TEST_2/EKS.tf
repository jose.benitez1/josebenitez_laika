resource "aws_eks_cluster" "Kubernetes_Laika_Testing" {
  name = var.cluster-name
  role_arn = aws_iam_role.EKS_Role_Cluster_Testing.arn
  #version = var.Version_EKS_Cluster
  vpc_config {
    subnet_ids = ["subnet-ec687597", "subnet-6daafc20", "subnet-7488531c"]
    security_group_ids = ["sg-0a34e4606f196b96f", "sg-0f46fa02e2a78e54e", "sg-071db46ab2437d0eb"]
    endpoint_private_access = "true"
    endpoint_public_access = "true"
  }
  tags = {
    Name = "Kubernetes_Laika_Testing"
    Enviroment = "Testing"
  }
  depends_on = [
    aws_iam_role_policy_attachment.Policy_EKS_Role_Cluster_Testing,
    aws_iam_role_policy_attachment.Service_EKS_Role_Cluster_Testing
  ]
}